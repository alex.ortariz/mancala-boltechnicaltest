package com.bol.assignment.mancala.service;

import static com.bol.assignment.mancala.util.MancalaConstants.*;

import org.springframework.stereotype.Service;

import com.bol.assignment.mancala.engine.MancalaEngine;
import com.bol.assignment.mancala.model.Game;
import com.bol.assignment.mancala.model.Player;
import com.bol.assignment.mancala.repository.GameRepository;

@Service
public class GameServiceImpl implements GameService{

	/**
	 * Initializing a game and storing it in the repository
	 * @throws Exception 
	 */
	public Game initOrJoinGame(String gameId) throws Exception {
		
		Game game = GameRepository.getGame(gameId);
		
		if (game == null) {
			game = new Game();
			game.setBoard(MancalaEngine.createInitialBoard());
			game.setPlayerHome(new Player("HOME"));
			game.setStatus(WAITING_RIVAL);
			GameRepository.createGame(gameId, game);
		}
		else
		{
			game.setStatus(TURN_HOME);
			game.setPlayerVisitor(new Player("VISITOR"));
		}
		
		return game;
	}
	
	/**
	 * Delete a game from the repository
	 * @throws Exception 
	 */
	public void deleteGame(String gameId) throws Exception {
		
		GameRepository.deleteGame(gameId);
		
	}

	

	/**
	 * Define the next movement, with the prior validations and rest of implications of the movement made(changing turn, finishing game, ...) 
	 */
	public Game nextMovement(int pitId, String gameId) {
		
		Game game = GameRepository.getGame(gameId);
		
		int resultValidation = MancalaEngine.validateMovement(pitId, game.getStatus(), game.getBoard());
		
		// If the movement is right, go on
		if (resultValidation == OK_CODE)
		{
			// Clean the error message
			game.setMessage("");

			// The position (pit) where the last seed has been put
			int lastIndex = MancalaEngine.distributeSeeds(pitId, game.getBoard());
			
			
			//The game is over as soon as one of the sides runs out of stones. The player who still 
			//has stones in his pits keeps them and puts them in his big pit. 
			//The winner of the game is the player who has the most stones in his big pit.
			if (MancalaEngine.checkEndAndSumRest(game)){
				MancalaEngine.calculateGameResult(game);
				game.setStatus(FINISHED);
				GameRepository.deleteGame(gameId);

				return game;
			};

			
			//If the player's last stone lands in his own big pit, he gets another turn
			// In other cases, the other player gets the turn
			checkAndchangeTurn(game, lastIndex);
			
		}
		else
		{
			game.setMessage(errorMap.get(resultValidation));
		}
		return game;
	
	}

	/**
	 * 	If the player's last stone lands in his own big pit, he gets another turn
	 *	In other cases, the other player gets the turn
	 * @param game game modified if it's needed
	 * @param lastIndex
	 */
	public void checkAndchangeTurn(Game game, int lastIndex) {
		
		if (TURN_HOME.equals(game.getStatus()))
		{
			if (lastIndex != PLAYER_HOME_BIG_PIT_POSITION) {
				game.setStatus(TURN_VISITOR);
			}
		}
		else if (TURN_VISITOR.equals(game.getStatus()))
		{
			if (lastIndex != PLAYER_VISITOR_BIG_PIT_POSITION) {
				game.setStatus(TURN_HOME);
			} 	
		}
		
	}
	
	

}
