package com.bol.assignment.mancala.service;

import com.bol.assignment.mancala.model.Game;

public interface GameService {

	public Game initOrJoinGame(String gameId) throws Exception;

	public Game nextMovement(int pitId, String gameId);
	
	public void deleteGame(String gameId) throws Exception;
	


	

	
}
