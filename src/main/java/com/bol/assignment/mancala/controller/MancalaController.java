package com.bol.assignment.mancala.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import com.bol.assignment.mancala.model.Game;
import com.bol.assignment.mancala.service.GameService;

@Controller
public class MancalaController {
	
	@Autowired
	GameService gameService;
	
	
	/**
	 * Creating or joining a game 
	 * @return Game with the data (Board, players, turn, etc.)
	 * @throws Exception
	 */
	@MessageMapping("/create")
	@SendTo("/topic/game")
	public Game initGame() throws Exception {
		// TODO --> Only one game is allowed. In the future, there will be multigames and multiqueues
		Game game = gameService.initOrJoinGame("ID");
		return game;
	}
	
	/**
	 * Delete the game
	 *
	 * @param gameId to be deleted
	 * @throws Exception
	 */
	@MessageMapping("/restart")
	@SendTo("/topic/game")
	public void delete() throws Exception {
		// TODO --> Only one game is allowed. In the future, there will be multigames and multiqueues
		gameService.deleteGame("ID");
	}

	
	/**
	 * Making the next movement
	 *
	 * @param pitId selected for the movement
	 * @return Game with the data (Board, players, turn, etc.)
	 * @throws Exception
	 */
	@MessageMapping("/movement")
	@SendTo("/topic/game")
	public Game move(int pitId) throws Exception {
		Game game = gameService.nextMovement(pitId, "ID");
		return game;
	}


}
