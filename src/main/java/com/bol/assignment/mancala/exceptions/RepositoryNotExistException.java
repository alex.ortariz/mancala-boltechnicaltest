package com.bol.assignment.mancala.exceptions;

/**
 * Exceptions caused when a game doesn't exist 
 */
public class RepositoryNotExistException extends Exception {

	public RepositoryNotExistException(String string) {
		super(string);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -1824838537331388301L;

}
