package com.bol.assignment.mancala.engine;

import static com.bol.assignment.mancala.util.MancalaConstants.BOARD_TOTAL_SIZE;
import static com.bol.assignment.mancala.util.MancalaConstants.DRAW;
import static com.bol.assignment.mancala.util.MancalaConstants.ERROR_CODE_PIT_EMTPY;
import static com.bol.assignment.mancala.util.MancalaConstants.ERROR_CODE_PIT_NOT_ALLOWED;
import static com.bol.assignment.mancala.util.MancalaConstants.ERROR_CODE_STILL_WAITING;
import static com.bol.assignment.mancala.util.MancalaConstants.HOME_WIN;
import static com.bol.assignment.mancala.util.MancalaConstants.OK_CODE;
import static com.bol.assignment.mancala.util.MancalaConstants.PLAYER_HOME_BIG_PIT_POSITION;
import static com.bol.assignment.mancala.util.MancalaConstants.PLAYER_VISITOR_BIG_PIT_POSITION;
import static com.bol.assignment.mancala.util.MancalaConstants.SEEDS_PER_PIT;
import static com.bol.assignment.mancala.util.MancalaConstants.TURN_HOME;
import static com.bol.assignment.mancala.util.MancalaConstants.TURN_VISITOR;
import static com.bol.assignment.mancala.util.MancalaConstants.VISITOR_WIN;
import static com.bol.assignment.mancala.util.MancalaConstants.WAITING_RIVAL;

import com.bol.assignment.mancala.model.Game;

/**
 * Implementation of Board and Game internal structure and functionality 
 */
public class MancalaEngine {

		
		/**
		 * Method that creates the initial board
		 */
		public static int[] createInitialBoard() {
			int[] board = new int[BOARD_TOTAL_SIZE];
			// Populating the board with 6 seeds for pit, except the big pits
			for (int i = 0; i < board.length; i++) {
				// Big pits have to be empty
				if (i != PLAYER_HOME_BIG_PIT_POSITION && i != PLAYER_VISITOR_BIG_PIT_POSITION)
					board[i] = SEEDS_PER_PIT;
			}
			return board;
		
		}
		
		/**
		 * Validate if the pit selected is property of the player that has the turn, is not a big pit, and is not empty  
		 * @param pitId
		 * @param status
		 * @param board
		 * @return related error code
		 */
		public static int validateMovement(int pitId, String status, int[] board) {
			if (status.equals(WAITING_RIVAL)) {
				return ERROR_CODE_STILL_WAITING;
			}
			// If the pit selected is not property of the player that has the turn, is a big pit, return related error code 
			if (!isPitSelectedAllowedForThisPlayer(pitId, status)) 
				return ERROR_CODE_PIT_NOT_ALLOWED;
			
			// If the pit has not seeds
			if(board[pitId] == 0)
				return ERROR_CODE_PIT_EMTPY;
			
			return OK_CODE;
		}
		
			
		/**
		 * Returns if is allowed the movement for the pit selected
		 *  Availability: 
		 *  	TURN HOME --> Positions 0 to 5 (NOT ITS BIG PIT)
		 *  	TURN AWAY --> Positions 7 to 12 (NOT ITS BIG PIT)
		 *  
		 * @param pitId
		 * @param status
		 * @return
		 */
		private static boolean isPitSelectedAllowedForThisPlayer(int pitId, String status) {
			if (0 <= pitId && pitId < PLAYER_HOME_BIG_PIT_POSITION && status.equals(TURN_HOME)
				|| (PLAYER_HOME_BIG_PIT_POSITION < pitId && pitId < PLAYER_VISITOR_BIG_PIT_POSITION && status.equals(TURN_VISITOR)))
				return true;
			else				
				return false;
		}


		/**
		 *	Distribute the seeds of the selected pit properly 
		 * @param pitId We get the seeds from this pit
		 * @param board 
		 * @param status
		 * @return the position (pit) where the last seed has been put  
		 */
		public static int distributeSeeds(int pitId, int[] board) {
				
			int numberOfSeeds = board[pitId];
			int index = pitId++;

			// Empty the pit selected, and after that, increase the index
			board[index++] = 0;

			while (numberOfSeeds != 0)
			{
				//No stones are put in the opponents' big pit (we determine who has the turn by the pitId chosen by the user)
				if (!((pitId < PLAYER_HOME_BIG_PIT_POSITION && index == PLAYER_VISITOR_BIG_PIT_POSITION)
					|| (pitId > PLAYER_HOME_BIG_PIT_POSITION && index == PLAYER_HOME_BIG_PIT_POSITION)))
				{
					 // During the game the pits are emptied on both sides. Always when the last stone lands in an own empty pit, the player captures his own stone and all stones in 
					 // the opposite pit (the other player’s pit) and puts them in his own (big or little) pit
					if (
						(numberOfSeeds == 1 && board[index] == 0) 
						&& ( (0 <= (index) && (index) <= PLAYER_HOME_BIG_PIT_POSITION && pitId < PLAYER_HOME_BIG_PIT_POSITION)
							|| (PLAYER_HOME_BIG_PIT_POSITION < (index) && (index) <= PLAYER_VISITOR_BIG_PIT_POSITION && pitId < PLAYER_VISITOR_BIG_PIT_POSITION) 
						   )
					   )
					   {
							// Get the opposite index (if they are the big pits, we don't have any formula. If they don't, we have a formula
							int oppositeIndex;
							if (index == PLAYER_HOME_BIG_PIT_POSITION)
							{
								oppositeIndex = PLAYER_VISITOR_BIG_PIT_POSITION;
							}
							else if (index == PLAYER_VISITOR_BIG_PIT_POSITION) 
							{
								oppositeIndex = PLAYER_HOME_BIG_PIT_POSITION;
							}
							else
							{
								oppositeIndex = Math.abs(index - 12);
							}
								
							board[index] = 1 + board[oppositeIndex];
							board[oppositeIndex] = 0;
					   }
					   else
					   {
						   // If the pit is not empty or it is not of the turns owner, we only put the seed 
						   board[index]++;
					   }
					
					// Decrease the number of seeds to put into the pits
					numberOfSeeds--;
				}
				
				
				// Increase the index or restart in case we reach the end of the board
				if (numberOfSeeds != 0) {
					if (index == PLAYER_VISITOR_BIG_PIT_POSITION)
					{
						index = 0;
					}
					else 
					{
						index++;
					}
				}
				

			}
			return index;
		}
		
		
		/**
		 * 	Run into the home/visitor pits. If there is one no zero, then return false. If don't,then return true and collect the opposite turn pit's seeds 
		 * @param board
		 * @return
		 */
		public static boolean checkEndAndSumRest(Game game) {

			if (TURN_HOME.equals(game.getStatus())){
				//Run into the home pits. If there is one no zero, return false
				for (int i=0 ; i < PLAYER_HOME_BIG_PIT_POSITION ; i++) {
					if (game.getBoard()[i] != 0) {
						return false;
					}
				}

				// All home pit's are 0. So, let's collect visitor's pit and let's add them to the visitor's big pit
				int total = 0;
				for (int j=PLAYER_HOME_BIG_PIT_POSITION+1 ; j < PLAYER_VISITOR_BIG_PIT_POSITION ; j++) {
					total+=game.getBoard()[j];
				}
				// Add them to the visitor's big pit 
				game.getBoard()[PLAYER_VISITOR_BIG_PIT_POSITION] += total;
					
			} else if (TURN_VISITOR.equals(game.getStatus())){
				//Run into the visitor pits. If there is one no zero, return false
				for (int i=PLAYER_HOME_BIG_PIT_POSITION+1 ; i < PLAYER_VISITOR_BIG_PIT_POSITION ; i++) {
					if (game.getBoard()[i] != 0) {
						return false;
					}
					
				}

				// All visitor pit's are 0. So, let's collect home's pit and let's add them to the home's big pit
				int total = 0;
				for (int j=0 ; j < PLAYER_HOME_BIG_PIT_POSITION ; j++) {
					total+=game.getBoard()[j];
				}
				// Add them to the home's big pit 
				game.getBoard()[PLAYER_HOME_BIG_PIT_POSITION] += total;
			}
			// Game ends
			return true;
		}

		/**
		 * Decide the winner, or draw
		 * @param game
		 */
		public static void calculateGameResult(Game game) {
			
			if (game.getBoard()[PLAYER_HOME_BIG_PIT_POSITION] > game.getBoard()[PLAYER_VISITOR_BIG_PIT_POSITION])
			{
				game.setMessage(HOME_WIN);
			}
			else if (game.getBoard()[PLAYER_HOME_BIG_PIT_POSITION] < game.getBoard()[PLAYER_VISITOR_BIG_PIT_POSITION])
			{
				game.setMessage(VISITOR_WIN);
			}
			else {
				game.setMessage(DRAW);
			}
		}

}

