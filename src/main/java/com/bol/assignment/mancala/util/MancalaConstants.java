package com.bol.assignment.mancala.util;

import java.util.HashMap;
import java.util.Map;

public class MancalaConstants {

	public static int BOARD_TOTAL_SIZE = 14;
	public static int PLAYER_HOME_BIG_PIT_POSITION = 6;
	public static int PLAYER_VISITOR_BIG_PIT_POSITION = 13;
	public static int SEEDS_PER_PIT = 6;
	

	public static String WAITING_RIVAL = "WAITING_RIVAL";
	public static String FINISHED = "FINISHED";
	public static String TURN_HOME = "TURN_HOME";
	public static String TURN_VISITOR = "TURN_VISITOR";
	
	public static int OK_CODE = 0;
	public static int ERROR_CODE_PIT_NOT_ALLOWED = -1;
	public static String ERROR_DESC_PIT_NOT_ALLOWED = "Pit selected is not allowed to make movement (pit selected is not property of the player that has the turn, or is a big pit)";
	public static int ERROR_CODE_STILL_WAITING = -2;
	public static String ERROR_DESC_STILL_WAITING = "Still waiting for rival";
	public static int ERROR_CODE_PIT_EMTPY = -3;
	public static String ERROR_DESC_PIT_EMTPY= "Pit empty";
	
	public static String HOME_WIN = "HOME PLAYER HAS WON!!!!!!!!!!!!";
	public static String VISITOR_WIN = "VISITOR PLAYER HAS WON!!!!!!!!!!!!";
	public static String DRAW = "DRAW. WE ARE ALL FRIENDS";
	
	public static Map<Integer, String> errorMap;
	static {
		errorMap = new HashMap<Integer, String>();
		errorMap.put(ERROR_CODE_PIT_NOT_ALLOWED, ERROR_DESC_PIT_NOT_ALLOWED);
		errorMap.put(ERROR_CODE_STILL_WAITING, ERROR_DESC_STILL_WAITING);
		errorMap.put(ERROR_CODE_PIT_EMTPY, ERROR_DESC_PIT_EMTPY);
	}
	

}
