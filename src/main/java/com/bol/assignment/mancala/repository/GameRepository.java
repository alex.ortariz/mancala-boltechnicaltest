package com.bol.assignment.mancala.repository;

import java.util.concurrent.ConcurrentHashMap;

import com.bol.assignment.mancala.exceptions.RepositoryNotExistException;
import com.bol.assignment.mancala.model.Game;

public class GameRepository {
	
	/** Singleton structure where games are stored */
	private static ConcurrentHashMap<String, Game> gameRepositoryMap;
	
	/**
	 * Add a game to the repository
	 * @param gameId
	 * @param game
	 */
	public static void createGame(String gameId, Game game)
	{
		if (gameRepositoryMap == null) {
			gameRepositoryMap = new ConcurrentHashMap<String, Game>();
		}
		gameRepositoryMap.put(gameId, game);	
	}
	
	/**
	 * Get a game from the repository
	 * @param gameId
	 * @return
	 * @throws RepositoryNotExistException 
	 */
	public static Game getGame(String gameId)
	{
		if (gameRepositoryMap == null) {
			return null;
		}
		else
		{
			return gameRepositoryMap.get(gameId);
		}
		
	}
	
	/**
	 * Remove the game from the repository
	 * @param gameId
	 */
	public static void deleteGame(String gameId) {
		gameRepositoryMap.remove(gameId);		
	}
	
}
