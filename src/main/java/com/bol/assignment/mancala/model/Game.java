package com.bol.assignment.mancala.model;

public class Game {
	
    private int[] board;
    private Player playerHome;
    private Player playerVisitor;
    private String status;
    private String message;

	public int[] getBoard() {
		return board;
	}
	
	public void setBoard(int[] board) {
		this.board = board;
	}
	
	public Player getPlayerHome() {
		return playerHome;
	}

	public void setPlayerHome(Player playerHome) {
		this.playerHome = playerHome;
	}

	public Player getPlayerVisitor() {
		return playerVisitor;
	}

	public void setPlayerVisitor(Player playerVisitor) {
		this.playerVisitor = playerVisitor;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
