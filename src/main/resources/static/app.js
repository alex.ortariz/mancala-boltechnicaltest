var stompClient = null;

function joingame() {
    var socket = new SockJS("/gs-guide-websocket");
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        $("#joingame").prop("disabled", true);
        $("#restart").prop("disabled", false);

        stompClient.subscribe("/topic/game", function (message) {
        	// Painting the board
        	bodyMessage = JSON.parse(message.body);
        	board = bodyMessage.board;
        	console.log("board is "+ board);
        	

            for (i=0; i<14; i++) {
            	$("#"+i).text(board[i]);
            }
            
            $("#status").text(bodyMessage.status);
            $("#message").text(bodyMessage.message);
            
            if (bodyMessage.status == "TURN_HOME")
            {
            	$(".btn-danger").prop("disabled", true);
            	$(".btn-primary").prop("disabled", false);

            }
            else if (bodyMessage.status == "TURN_VISITOR")
            {
            	$(".btn-primary").prop("disabled", true);
            	$(".btn-danger").prop("disabled", false);
            }

        });
        stompClient.send("/app/create", {}, {})
    });
}

function remove() {
    stompClient.send("/app/restart", {});
    joingame();
}


function move(pitID) {
    stompClient.send("/app/movement", {}, pitID);
}


$(function () {
	$(".col-sm-1").prop("disabled", true);

	$("form").on("submit", function (e) {
        e.preventDefault();
    });
	
    $( "#joingame" ).click(function() 
    {
    	joingame(); 
    });
    
    $( "#restart" ).click(function() 
    {
    	remove(); 
    });
    $("#restart").prop("disabled", true);

    


});