package com.bol.test.assignment.mancala;

import static com.bol.assignment.mancala.util.MancalaConstants.PLAYER_HOME_BIG_PIT_POSITION;
import static com.bol.assignment.mancala.util.MancalaConstants.PLAYER_VISITOR_BIG_PIT_POSITION;
import static com.bol.assignment.mancala.util.MancalaConstants.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.bol.assignment.mancala.engine.MancalaEngine;
import com.bol.assignment.mancala.model.Game;

public class MancalaEngineTest {
	
	
	// Testing createInitialBoard (fullfilling)
	@Test
	public void initGame() throws Exception {
		int [] board = MancalaEngine.createInitialBoard();
		// Correct fullfill of pits
		for (int i = 0; i < board.length; i++) {
			if (i == PLAYER_HOME_BIG_PIT_POSITION || i == PLAYER_VISITOR_BIG_PIT_POSITION) {
				assertEquals(board[i],0);
			}
			else
			{
				assertEquals(board[i],SEEDS_PER_PIT);
			}
		}
		
		//Not equals
		assertNotEquals(board[5],7);

	}
	
	
	// Testing validate different movements 
	@Test
	public void validateMovement() throws Exception {
		int [] board = MancalaEngine.createInitialBoard();
		
		// Not allowed
		assertEquals(ERROR_CODE_PIT_NOT_ALLOWED,MancalaEngine.validateMovement(2, TURN_VISITOR, board));
		// Allowed
		assertEquals(OK_CODE,MancalaEngine.validateMovement(2, TURN_HOME, board));
		// Not allowed
		board[2]=0;
		assertEquals(ERROR_CODE_PIT_EMTPY,MancalaEngine.validateMovement(2, TURN_HOME, board));

	}


	// Testing checkEndAndSumRest 
	@Test
	public void checkEndAndSumRest() throws Exception {
		int [] board = MancalaEngine.createInitialBoard();
		Game game = new Game();
		game.setBoard(board);
		game.setStatus(TURN_HOME);
		// Not allowed
		assertFalse(MancalaEngine.checkEndAndSumRest(game));

		for (int i = 0; i < 6; i++) {
			board[i]=0;
			game.setStatus(TURN_HOME);
		}
		// Allowed
		assertTrue(MancalaEngine.checkEndAndSumRest(game));

	}

	// Testing distributeSeeds (next movement of the seed and board disposition) 
	@Test
	public void distributeSeeds() throws Exception {
		int [] board = MancalaEngine.createInitialBoard();
		Game game = new Game();
		game.setBoard(board);
		game.setStatus(TURN_HOME);
		// 6
		assertEquals(6,MancalaEngine.distributeSeeds(0,board));

		// 8 
		assertNotEquals(8,MancalaEngine.distributeSeeds(2,board));

		// Restart board
		board = MancalaEngine.createInitialBoard();
		// 9
		assertNotEquals(9,MancalaEngine.distributeSeeds(2,board));

	}

	
	// Testing calculateGameResult 
	@Test
	public void calculateGameResult() throws Exception {
		int [] board = MancalaEngine.createInitialBoard();
		Game game = new Game();
		game.setBoard(board);
		game.setStatus(TURN_HOME);
		
		MancalaEngine.calculateGameResult(game);

		//Draw
		assertEquals(DRAW,game.getMessage());
		
		// More seeds in the home
		MancalaEngine.distributeSeeds(0,board);
		MancalaEngine.calculateGameResult(game);
		// Home Win 
		assertEquals(HOME_WIN,game.getMessage());

		game.setStatus(TURN_VISITOR);
		MancalaEngine.distributeSeeds(7,board);
		MancalaEngine.distributeSeeds(8,board);
		MancalaEngine.calculateGameResult(game);
		// Visitor Win 
		assertEquals(VISITOR_WIN,game.getMessage());

	}

}
