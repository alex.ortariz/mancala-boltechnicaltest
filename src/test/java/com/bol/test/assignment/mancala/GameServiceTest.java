package com.bol.test.assignment.mancala;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import com.bol.assignment.mancala.model.Game;
import com.bol.assignment.mancala.repository.GameRepository;
import com.bol.assignment.mancala.service.GameService;
import com.bol.assignment.mancala.service.GameServiceImpl;
import com.bol.assignment.mancala.util.MancalaConstants;

public class GameServiceTest {
	
	private GameService gameService;

	
    @Before
    public void setUp() throws Exception {
    	gameService = new GameServiceImpl();
    }

	// Testing init game
	@Test
	public void initGame() throws Exception {
		gameService.initOrJoinGame("ID");
		// This exists
		assertTrue(GameRepository.getGame("ID")!=null);
		// This not exists
		assertFalse(GameRepository.getGame("asdsadsadassd")!=null);

	}
	
	// Testing delete game
	@Test
	public void deleteGame() throws Exception {
		gameService.initOrJoinGame("ID");
		// This exists
		assertTrue(GameRepository.getGame("ID")!=null);
		// Deleting game
		gameService.deleteGame("ID");
		// This not exists
		assertFalse(GameRepository.getGame("ID")!=null);
	}

	// Testing next movement (ERROR STILL WAITING)
	@Test
	public void nextMovementStillWating() throws Exception {
		Game game = gameService.initOrJoinGame("ID");
		game = gameService.nextMovement(0, "ID");
		// Error still waiting
		assertEquals(game.getMessage(), MancalaConstants.ERROR_DESC_STILL_WAITING);
	}


}
