## MINIMUM EXECUTION REQUIREMENTS

1.- Java 1.8
2.- Web Browser


## EXECUTION INSTRUCTIONS

1.-You can execute jar file 'java -jar mancala-0.0.1-SNAPSHOT.jar' (embedded tomcat web app server)
2.-You can execute ApplicationStarter class from your IDE (you can change port and context in application.properties)
3.-Once started, you can operate via via web browser, typing the URL (localhost:8080 by default)


## APPLICATION DESCRIPTION

Web based Mancala game. You can play it through a web browser, typing localhost:8080 (by default).
Two people can play the game in one computer, or in two separate computers. One has to start the game and the other, then, press the same button (in other browser or computer) to join the game).
Executed in one server (one instance), it is prepared only for one game at the same time.
You can restart the game. If so, the rival have to refresh the web browser, and then, join the game.    	
	
## TECHNOLOGY STACK
- Java 1.8
- Spring Boot 
- Spring MVC
- Spring WebSockets
- Bootstrap
- Maven


## ASSUMPTIONS AND DESIGN EXPLANATION

I have assumed (taking into account the specs) that I have to focus on backend, so I have no made a big effort in frontend results presentation.
I tried to do this as simple as possible. Not over-engineering or overcoding.
The most difficult part to decide was the management of the web sockets. Many interesting things could be done with this technology.
I decided not to persist the games information, so I decided to do it in an easy way, storing them in memory. 
So, I have divided the app into 3 layers: controller, service and MancalaEngine (the responsible of game calculations).
The communication between the client and the server has been made by websockets.
Moreover, I perform a test suite too, taking into account many possibilities and errors.

## IMPROVEMENTS

If I had had more time:
* Multigaming repository system.
* Communication with the concrete user by web sockets.
* Use of a design patter like observer or chain of responsability, maybe.
* Better UI.
* Persistence layer (Mongo, H2, or something more simple)
* More testing
* Logging